angular.module("umbraco")
    .controller("My.MarkMarkController",
    function ($scope,assetsService) {

	if($scope.model.value === null || $scope.model.value === ""){
    	$scope.model.value = $scope.model.config.defaultValue;
	}

    //tell the assetsService to load the markdown.editor libs from the markdown editors
    //plugin folder
    assetsService
        .load([
            "/App_Plugins/MarkMark/lib/Markdown.Converter.js",
            "/App_Plugins/MarkMark/lib/Markdown.Sanitizer.js",
            "/App_Plugins/MarkMark/lib/Markdown.Editor.js"
        ])
        .then(function () {
            //this function will execute when all dependencies have loaded
            var converter2 = new Markdown.Converter();
			var editor2 = new Markdown.Editor(converter2, "-" + $scope.model.alias);
			editor2.run();
            alert("editor dependencies loaded");
        });

    //load the separate css for the editor to avoid it blocking our js loading
    assetsService.loadCss("/App_Plugins/MarkMark/lib/Markdown.Editor.less");
});